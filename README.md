# Fit_irrad

Tool to fit crystal transparency loss in ECAL, assuming different types of color centers creation/annihilation/saturation processes.
Use recorded instantaneous luminosity to do it.